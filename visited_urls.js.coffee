window.visitedUrl =
  reg: null
  init: ->
    console.log "============visitedUrl============"
    @markVisitedThisPage()
    @hideVisitedUrls()
    $('body').on 'mouseup click', 'a', (e) ->
      if e.which == 3
        return
      link = $(e.target).closest('a').attr('href')
      if ! !link and link != 'javascript:void(0)'
        window.visitedUrl.markVisited link
      return
    return
  markVisited: (url) ->
    console.log "before modify============#{url}============"
    if @modifyUrl
      url = @modifyUrl(url)
    console.log "after modify============#{url}============"
    return unless url
    url = url.replace(/\#.+/, '')
    if url.replace(/\/$/, '') == location.origin
      return
    if @reg and !@reg.test(url)
      console.log 'hut roi'
      return
    visitedUrls = localStorage.getObj('visitedUrls') or []
    if visitedUrls.indexOf(url) == -1
      visitedUrls.push url
    localStorage.setObj 'visitedUrls', visitedUrls
    @hideVisitedUrls [ url ]
    return
  markVisitedThisPage: ->
    @markVisited location.href
    @markVisited location.pathname.replace(/^\/diendan\//, '')
    return
  hideVisitedUrls: (visitedUrls = null) ->
    switch location.hostname
      when 'stackoverflow.com'
        timer = 1000
      else
        timer = 0
    setTimeout (->
      visitedUrls = visitedUrls or localStorage.getObj('visitedUrls') or []
      $.each visitedUrls, (i, url) ->
        els = $('a[href^=\'' + url + '\']')
        els.each (i, e) ->
          if $(e).attr('href').replace(/\#.+/, '') == url
            window.visitedUrl.hideUrl $(e)
          return
        return
      return
    ), timer
    return
  hideUrl: (e) ->
    console.log e.attr('href')
    switch location.hostname
      when 'stackoverflow.com'
        console.log e
        e.closest('.dno').remove()
        e.remove()
      when 'www.imagefap.com'
        tr = e.closest('tr')
        tr.next('tr').remove()
        tr.remove()
      when 'thiendia.com'
        e.closest('.discussionListItem').remove()
      else
        $(e).remove()
    return
  addException: ->
    return

  modifyUrl: (url)->
    if @modifyUrlRegex
      if url.match(@modifyUrlRegex, "")
        url = url.match(@modifyUrlRegex, "")[0]
        url = "/" + url unless url.indexOf("http") == 0
        console.log "match============#{url}============"
        return url
      else
        console.log "not match============#{url}============"
        return null
    else
      return url
