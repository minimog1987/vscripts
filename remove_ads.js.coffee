general.addBtn "Report", "general.report"
general.addBtn "Clear Inteval", "general.clearAllIntervals", 2
switch document.domain
  when "tmearn.com"
    $(".banner-inner").remove()
    $(".adsbygoogle").remove()
    unsafeWindow.$(document).off "click"
    unsafeWindow.$(document).off "focusout"
    unsafeWindow.$(document).off "blur"
  when "javidolvideo.blogspot.com"
    general.cloneBody()
    general.addBtn "Report", "general.report"
    general.addBtn "Clear Inteval", "general.clearAllIntervals", 2
  when "openload.co"
    unsafeWindow.$(document).off "click"
    unsafeWindow.$(document).off "keydown"
