# @require     file:///C:/Users/mog/Documents/scripts/underscore.js

window.bt =
  safeRate: null
  els:
    sellMaxBtn: "#form_Sell button[data-bind^='click: trade.sellMax']"
    sellPriceInput: "#form_Sell input[data-bind='value: trade.sellPrice']"
    sellNoConditionBtn: "#form_Sell a[data-bind='text:label, click:$parent.trade.setSellOrderType']:eq(0)"
    sellConditionBtn: "#form_Sell a[data-bind='text:label, click:$parent.trade.setSellOrderType']:eq(1)"
    sellConditionLessBtn: "#form_Sell a[data-bind='text: label, click: $parent.trade.setSellCondition']:eq(2)"
    sellConditionLessValInput: "#form_Sell input[data-bind='value: trade.sellTarget']"
    sellBtn: "#form_Sell button[type=submit]"

    buyMaxBtn: "#form_Buy button[data-bind^='click: trade.buyMax']"
    buyUnitsInput: "#form_Buy input[data-bind='value: trade.buyVolume, attr: { readonly: trade.buyMaxEnabled() }']"
    buyPriceInput: "#form_Buy input[data-bind='value: trade.buyPrice']"
    buyNoConditionBtn: "#form_Buy a[data-bind='text:label, click:$parent.trade.setBuyOrderType']:eq(0)"
    buyConditionBtn: "#form_Buy a[data-bind='text:label, click:$parent.trade.setBuyOrderType']:eq(1)"
    buyConditionGreaterBtn: "#form_Buy a[data-bind='text: label, click: $parent.trade.setBuyCondition']:eq(1)"
    buyConditionGreaterValInput: "#form_Buy input[data-bind='value: trade.buyTarget']"
    buyBtn: "#form_Buy button[type=submit]"

  init: ->
    @detectCoin()
    general.addBtn("Ban Tren", "bt.banTren", 1)
    general.addBtn("Mua Duoi", "bt.muaDuoi", 2)
    @safeRate = localStorage.getItem("bt#{@coin}SafeRate")
    @fivePercentRate = localStorage.getItem("bt#{@coin}FivePercentRate")
    @tenPercentRate = localStorage.getItem("bt#{@coin}TenPercentRate")
    general.addBtn("Rates: safe: #{@safeRate}, 5%: #{@fivePercentRate}, 10%: #{@tenPercentRate}", "bt.updateRates", 3)
    general.addInput("Step", 4)
    general.addBtn("Ban Ngay", "bt.banNgay", 5)
    general.addRadio("Sell Rate", "sell_rate", ["last", "ask", "safe", "five", "ten"], 6)
    general.addBtn("Mua Ngay", "bt.muaNgay", 7)
    general.addRadio("Buy Rate", "buy_rate", ["last", "bid"], 8)
    general.addRadio("Buy Rate", "buy_val", ["max", "one_unit"], 9)
    $('#helperInput4').val(localStorage.getItem("bt#{@coin}Step"));

  detectCoin: ->
    @coin = window.location.href.split("-")[1]
    return

  updateRates: ->
    @safeRate = Number($('#closedMarketOrdersTable tbody tr').filter((i, e) ->
        $(e).find('td:eq(2)').text() == 'Limit Buy'
    ).first().find('td:eq(3)').text()) * 1.01
    @safeRate = Math.round(@safeRate * 100000000) / 100000000
    @fivePercentRate = Math.round(@safeRate * 1.05 * 100000000) / 100000000
    @tenPercentRate = Math.round(@safeRate * 1.1 * 100000000) / 100000000

    localStorage.setItem("bt#{@coin}SafeRate", @safeRate)
    localStorage.setItem("bt#{@coin}FivePercentRate", @fivePercentRate)
    localStorage.setItem("bt#{@coin}TenPercentRate", @tenPercentRate)
    $("#helper3").text "Rates: safe: #{@safeRate}, 5%: #{@fivePercentRate}, 10%: #{@tenPercentRate}"

  banTren: ->
    try
      @getStep()
      $(@els.sellMaxBtn).trigger("click")
      unsafeWindow.$(@els.sellConditionBtn).trigger("click")
      unsafeWindow.$(@els.sellConditionLessBtn).trigger("click")
      unsafeWindow.$(@els.sellPriceInput).val(@sellValRate()).trigger("blur").trigger("change")
      unsafeWindow.$(@els.sellConditionLessValInput).val(@sellPointRate()).trigger("blur").trigger("change")
      unsafeWindow.$(@els.sellBtn).trigger("click")
    catch err
      alert err.message
    return

  currentLast: ->
    return Math.round(Number($("span[data-bind='text: summary.displayLast()']").text()) * 100000000) / 100000000

  currentAsk: ->
    return Math.round(Number($("span[data-bind='text: summary.displayAsk()']").text()) * 100000000) / 100000000

  currentBid: ->
    return Math.round(Number($("span[data-bind='text: summary.displayBid()']").text()) * 100000000) / 100000000

  getStep: ->
    @step = Number($("#helperInput4").val())
    localStorage.setItem("bt#{@coin}Step", @step)
    return @step

  sellPointRate: ->
    return Math.round((@currentLast() - @step * 2) * 100000000) / 100000000

  sellValRate: ->
    return Math.round((@currentLast() - @step * 3) * 100000000) / 100000000

  muaDuoi: ->
    try
      @getStep()
      $(@els.buyMaxBtn).trigger("click")
      unsafeWindow.$(@els.buyConditionBtn).trigger("click")
      unsafeWindow.$(@els.buyConditionGreaterBtn).trigger("click")
      unsafeWindow.$(@els.buyPriceInput).val(@buyValRate()).trigger("blur").trigger("change")
      unsafeWindow.$(@els.buyConditionGreaterValInput).val(@buyPointRate()).trigger("blur").trigger("change")
      unsafeWindow.$(@els.buyBtn).trigger("click")
    catch err
      alert err.message
    return

  buyPointRate: ->
    return Math.round((@currentLast() + @step * 2) * 100000000) / 100000000

  buyValRate: ->
    return Math.round((@currentLast() + @step * 3) * 100000000) / 100000000

  banNgay: ->
    rateKey = $("[name=sell_rate]:checked").val()
    switch rateKey
      when "last"
        rate = @currentLast()
      when "ask"
        rate = @currentAsk()
      when "safe"
        rate = @safeRate
      when "five"
        rate = @fivePercentRate
      when "ten"
        rate = @tenPercentRate
    try
      $(@els.sellMaxBtn).trigger("click")
      unsafeWindow.$(@els.sellNoConditionBtn).trigger("click")
      unsafeWindow.$(@els.sellPriceInput).val(rate).trigger("blur").trigger("change")
      unsafeWindow.$(@els.sellBtn).trigger("click")
    catch err
      alert err.message
    return

  muaNgay: ->
    rateKey = $("[name=buy_rate]:checked").val()
    valKey = $("[name=buy_val]:checked").val()
    switch rateKey
      when "last"
        rate = @currentLast()
      when "bid"
        rate = @currentBid()
    unsafeWindow.$(@els.buyPriceInput).val(rate).trigger("blur").trigger("change")
    if $(@els.buyMaxBtn).hasClass("active")
      $(@els.buyMaxBtn).trigger("click")
    switch valKey
      when "max"
        $(@els.buyMaxBtn).trigger("click")
      when "one_unit"
        buyVal = Math.round((0.02 / rate) * 100000000) / 100000000
        unsafeWindow.$(@els.buyUnitsInput).val(buyVal).trigger("blur").trigger("change")
    try
      unsafeWindow.$(@els.buyNoConditionBtn).trigger("click")
      unsafeWindow.$(@els.buyBtn).trigger("click")
    catch err
      alert err.message
    return


window.bt.init()