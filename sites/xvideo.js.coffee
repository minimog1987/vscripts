general.convertTitle();
visitedUrl.modifyUrl = (url)->
  if url.match(/video\d+/, "")
    url = url.match(/video\d+/, "")[0]
    url = "/" + url
    return url
  else
    return null
visitedUrl.hideVisitedUrls = (visitedUrls = null) ->
  setTimeout (->
    visitedUrls = visitedUrls or localStorage.getObj('visitedUrls') or []
    $.each visitedUrls, (i, url) ->
      els = $('a[href^=\'' + url + '\']')
      els.remove()
    return
  ), 100
  return
visitedUrl.init();