general.convertTitle()
switch document.domain
  when "ngamvn.net"
    visitedUrl.modifyUrlRegex = /photo\/\d+/
    visitedUrl.hideVisitedUrls = (visitedUrls = null) ->
      setTimeout (->
        visitedUrls = visitedUrls or localStorage.getObj('visitedUrls') or []
        $.each visitedUrls, (i, url) ->
          els = $('a[href^=\'' + url + '\']')
          els.closest(".items").remove()
        return
      ), 100
      return
    visitedUrl.init()
    general.clickAllSelector = "#contents h3 a"
    general.addBtn("click ALL", "general.clickAll", 1)
    $(".top-header").remove()
    $(".breadcrumbs").remove()
    $(".likeButton").remove()
    $(".stats").remove()
    $(".utilities-bar").remove()
    $("#bar_float_r").remove()
    $("#bar_float").remove()
    $("#ad_left").remove()
    $("#ad_right").remove()
    $(".adsTopHome").remove()
    $(".banner-topR").remove()
  when "dethuongvl.com"
    visitedUrl.modifyUrlRegex = /http\:\/\/dethuongvl.com\/media\/.+/
    visitedUrl.hideVisitedUrls = (visitedUrls = null) ->
      if $("#media").length == 0
        $("#sidebar").remove()
      else
        setTimeout (->
          visitedUrls = visitedUrls or localStorage.getObj('visitedUrls') or []
          $.each visitedUrls, (i, url) ->
            els = $('a[href^=\'' + url + '\']')
            els.closest(".item").remove()
          unsafeWindow.$("#media").masonry("reloadItems")
          unsafeWindow.$("#media").masonry("layout")
          return
        ), 100
      return
    visitedUrl.init()
    general.clickAllSelector = "#media .item .item-title a[href^='http://dethuongvl.com/media/']"
    general.addBtn("click ALL", "general.clickAll", 1)
    general.addBtn("remote Visited", "visitedUrl.hideVisitedUrls", 2)
  when "www.imagefap.com"
    visitedUrl.reg = /(http:\/\/www.imagefap.com)?\/gallery.php\?gid.+/
    visitedUrl.init()
    if general.matchUrl("http://www.imagefap.com/pictures/.+")
      general.removeNotNeededJS()
      getImages.init()
      getImages.getFullImagesOnly()
      slider.init()
  when "www.instagram.com", "instagram.com"
    getImages.findImages = ->
      imgs = document.getElementsByClassName('_cmdpi')[0].getElementsByTagName('img')
      getImages.images = _.map(imgs, (img) ->
        full = img.src.replace('s640x640\/sh0\.08\/e35\/', 'e35/')
        full = img.src.replace('\/c0.+\/', '/')
        {
          id: img.src.split('/').pop().split('.')[0]
          full: full
        }
      )

    getImages.init()
  when "www.hongfire.com"
    general.addBtnFunc "Open All", 1, -> $("input[value='Show']").trigger("click")