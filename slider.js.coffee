window.slider =
  images: []
  timer: 3000
  sliding: false
  interval: null
  page: null
  els:
    status: "#helper3"
    nextIcon: "<div style='background: url(https://cdn3.iconfinder.com/data/icons/faticons/32/arrow-right-01-128.png); height: 128px; width: 128px; cursor: pointer; position: fixed; right: 10px; top: 50%;' id='next-slide'></div"
    prevIcon: "<div style='background: url(https://cdn3.iconfinder.com/data/icons/faticons/32/arrow-left-01-128.png); height: 128px; width: 128px; cursor: pointer; position: fixed; left: 10px; top: 50%;' id='prev-slide'></div"
  init: ->
    general.addBtn "Sliding", "slider.toogleSlide", 5, "toggle-slide"
    general.addBtn "+ Timer", "slider.increaseTimer", 6, "incr-timer"
    general.addBtn "- Timer", "slider.decreaseTimer", 7, "decr-timer"
    @initKeys()
    $("body").on "click", ".view-mode", (e)->
      if $(e.target).is(".view-mode")
        slider.closeModal()
    $("body").on "click", "img", (e)-> return slider.showImage $(e.target)
    $("body").on "click", "#next-slide", (e)-> return slider.nextSlide()
    $("body").on "click", "#prev-slide", (e)-> return slider.prevSlide()
    $("body").css("-moz-user-select", "none")
    @updateView()
    return

  initKeys: ->
    $(window).keypress (e) ->
      return unless $('.view-mode').length > 0
      # space
      if e.keyCode == 0 or e.keyCode == 32
        e.preventDefault()
        slider.toogleSlide()
      else if e.keyCode == 27
        # escape
        slider.closeModal()
      else if e.keyCode == 39 or e.keyCode == 34
        # -> pgdn
        slider.nextSlide()
      else if e.keyCode == 37 or e.keyCode == 33
        # -> pgdn
        slider.prevSlide()
      else if e.keyCode == 43
        # -> +
        slider.increaseTimer()
      else if e.keyCode == 45
        # -> -
        slider.decreaseTimer()
      return
    return

  openModal: ->
    if $('.view-mode').length == 0
      $('body').append '<div class="view-mode" style="position: fixed; width: 100%; height: 100%; left: 0px; top: 0px; background-color: rgba(0, 0, 0, 0.9); text-align: center;"></div>'
      $("body .view-mode").append($(@els.nextIcon))
      $("body .view-mode").append($(@els.prevIcon))

  closeModal: ->
    @stopSlide()
    $('.view-mode').remove()
    return

  increaseTimer: ->
    @timer += 1000
    @updateView()
    return

  decreaseTimer: ->
    @timer -= 1000
    @timer = 500 if @timer <= 500
    @updateView()
    return

  startSlide: ->
    @sliding = true
    @updateView()
    slider.nextSlide()
    return

  stopSlide: ->
    @sliding = false
    @updateView()
    clearInterval @interval
    return

  nextSlide: ->
    @page = Number(@page) + 1
    image = $('img:eq(' + @page + ')')
    @showImage image
    clearInterval @interval
    if @sliding
      @interval = setInterval((->
        slider.nextSlide()
        return
      ), @timer)
    return

  prevSlide: ->
    @page = Number(@page) - 1
    image = $('img:eq(' + @page + ')')
    @showImage image
    clearInterval @interval
    return

  toogleSlide: ->
    if @sliding
      @stopSlide()
    else
      @startSlide()
    return

  showImage: (image)->
    @openModal()
    @page = image.attr('data-index')
    $('.view-mode img').remove()
    $('.view-mode').append image.clone()
    $('.view-mode img').css 'float', 'none'
    $('.view-mode img').css 'height', '100%'
    $('.view-mode img').css 'width', 'auto'
    general.clearSelection()
    return

  updateView: ->
    if @sliding
      $("#toggle-slide").text "Sliding - #{@timer}"
      $("#incr-timer").show()
      $("#decr-timer").show()
    else
      $("#toggle-slide").text "No slide - #{@timer}"
      $("#incr-timer").hide()
      $("#decr-timer").hide()