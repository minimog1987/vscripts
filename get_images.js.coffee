window.getImages =
  totalLoading: 3
  loadingCounter: 0
  images: []
  init: ->
    general.addBtn "Get Images", "getImages.getFullImagesOnly", 1
    return

  addMoreBtns: ->
    general.addBtn "Reload", "getImages.reload", 2
    general.addBtn "Status", "", 3, "status"
    general.addBtn "Get Links", "getImages.getLinks", 4
    return

  findImages: ->
    switch document.domain
      when 'www.imagefap.com'
        imgs = document.getElementById('gallery').getElementsByTagName('img')
        thumbRegex = /(.*\/images\/thumb\/.*)\/(.*)$/i
        imgs = _.filter(imgs, (img) ->
          thumbRegex.test img.src
        )
        @images = _.map(imgs, (img) ->
          {
            id: img.src.split('/').pop().split('.')[0]
            full: img.src.replace('thumb', 'full')
          }
        )
        return

  reloadImage: (img) -> return img.setAttribute 'src', img.getAttribute('data-origin-src') + '?a=' + new Date

  getLinks: ->
    $('.imgs').html ''
    $('.imgs').css 'padding-left', '100px'
    $('.imgs').css 'padding-top', '100px'
    $('.imgs').append '<span>' + document.title + '</span>'
    $('.imgs').append '<br/>'
    _.each @images, (img) ->
      $('.imgs').append '<span>' + img.full + '</span>'
      $('.imgs').append '<br/>'
    return

  reload: ->
    errorImages = $('img').filter((i, a) ->
      a.naturalHeight == 0
    )
    $.each errorImages, ((_this) ->
      (index, img) ->
        _this.reloadImage img
        return
    )(this)
    return

  loadImage: (img)->
    if @loadingCounter <= @totalLoading
      @loadingCounter += 1
      imgSrc = img.getAttribute('data-src')
      img.removeAttribute 'data-src'
      img.setAttribute 'src', imgSrc
      img.onload = ((_this) ->
        ->
          _this.loadingCounter -= 1
          img.removeAttribute 'data-error'
          _this.loadImages()
          $('.imgs').isotope 'layout'
          return
      )(this)
      img.onerror = ((_this) ->
        ->
          img.setAttribute 'data-error', true
          _this.loadingCounter -= 1
          _this.loadImages()
          return
      )(this)
    return

  loadImages: ->
    $.each $('img[data-src]:lt(' + @totalLoading + ')'), ((_this) ->
      (index, img) ->
        _this.loadImage img
        return
    )(this)
    if @loadingCounter > 0
      $('#status').text 'loading'
    else
      $('#status').text 'load done'
    return

  getFullImagesOnly: ->
    _this = this
    @findImages()
    $('body').html ''
    @addMoreBtns()
    $('body').append '<div class=\'imgs\'></div>'
    _.each @images, (image, index) ->
      img = undefined
      link = undefined
      img = document.createElement('img')
      $(img).attr 'data-index', index
      $(img).attr 'data-src', image.full
      $(img).attr 'data-origin-src', image.full
      $(img).css 'float', 'left'
      $(img).css 'width', '100%'
      $(img).css 'background-color', 'blue'
      $(img).css 'box-sizing', 'border-box'
      $(img).css 'min-height', '30px'
      link = document.createElement('a')
      link.className = 'image'
      # link.href = image.full;
      link.appendChild img
      link.style.width = '20%'
      link.style.boxSizing = 'border-box'
      link.style.padding = '8px'
      $('.imgs').append link
    @loadImages()
    $('.imgs').isotope()
    return