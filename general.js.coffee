Storage::setObj = (key, obj) ->
  @setItem key, JSON.stringify(obj)

Storage::getObj = (key) ->
  JSON.parse @getItem(key)

window.general =
  removeNotNeededJS: ->
    scripts = undefined
    $('script[async]').remove()
    scripts = document.getElementsByTagName('script')
    _.each scripts, (script) ->
      if script and !/jquery\.min\.js/i.test(script.src)
        console.log 'Killed ', script.src
        return script.parentNode.removeChild(script)
      return
    return
  convertTiengViet: (aa) ->
    aa = aa.toLocaleLowerCase()
    aa = aa.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    aa = aa.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, 'e')
    aa = aa.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    aa = aa.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g, 'o')
    aa = aa.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    aa = aa.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    aa = aa.replace(/đ/g, 'd')
    aa = aa.replace(/\s/g, '_')
    aa
  convertTitle: ->
    document.title = @convertTiengViet(document.title)
    return
  addBtn: (text, func, pos = 1, id = null) ->
    id ||= "helper#{pos}"
    top = pos * 30
    $('body').prepend "<button id='#{id}' style='position: fixed; top: #{top}px; left:0; z-index:999999'>#{text}</button>"
    $("##{id}").on "click", -> eval("#{func}()")
    return
  addBtnFunc: (text, pos = 1, func) ->
    id = "helper#{pos}"
    top = pos * 30
    $('body').prepend "<button id='#{id}' style='position: fixed; top: #{top}px; left:0; z-index:999999'>#{text}</button>"
    $("##{id}").on "click", -> func()
    return
  addInput: (placeholder, pos = 1) ->
    top = pos * 30
    $('body').prepend '<input id="helperInput' + pos + '" style=\'position: fixed; top:' + top + 'px; left:0; z-index:999999; width: 100px\' placeholder=\'' + placeholder + '\'>'
    return
  addRadio: (label, key, vals, pos = 1) ->
    top = pos * 30
    html = """
      <div id='#{key}-div' style=\'position: fixed; top: #{top}px; left:0; z-index:999999; background-color: yellow;\' placeholder=\'' + placeholder + '\'>
        <span>#{label}:</span>
    """
    _.each vals, (val, i)->
      if i == 0
        html += "<input name='#{key}' value='#{val}' type='radio' id='#{key}_#{val}' checked=true>"
      else
        html += "<input name='#{key}' value='#{val}' type='radio' id='#{key}_#{val}'>"
      html += "<label for='#{key}_#{val}'>#{val}</label>"
    html += "</div>"
    $('body').prepend html
    return
  matchUrl: (url) ->
    RegExp(url).test location.href

  clickAll: ->
    $(@clickAllSelector).each (i, e)->
      $(e).trigger("click")
      window.open($(e).attr('href'), '_blank')
    $("body").scrollTop(9999)
    $("html").scrollTop(9999)
    return

  clearSelection: ->
    if document.selection
      document.selection.empty()
    else if window.getSelection
      window.getSelection().removeAllRanges()
    return

  report: (selector, root)->
    s = []
    total = 0
    unsafeWindow.$(selector or '*', root).addBack().each ->
      e = unsafeWindow.$._data(this, 'events')
      if !e
        return
      tagGroup = @tagName or 'document'
      if @id
        tagGroup += '#' + @id
      if @className
        tagGroup += '.' + @className.replace(RegExp(' +', 'g'), '.')
      delegates = []
      for p of e
        r = e[p]
        h = r.length - (r.delegateCount)
        if h
          s.push [
            tagGroup
            p + ' handler' + (if h > 1 then 's' else '')
            h
          ]
          total += h
        if r.delegateCount
          q = 0
          while q < r.length
            if r[q].selector
              s.push [
                tagGroup + ' delegates'
                p + ' for ' + r[q].selector
                r.delegateCount
              ]
              total += r.delegateCount
            q++
      return
    console.log {total: total, details: s}
    return

  clearAllIntervals: ->
    highestTimeoutId = setTimeout(';')
    i = 0
    while i < highestTimeoutId
      clearTimeout i
      i++

  cloneBody: (el = document.body)->
    elClone = el.cloneNode(true)
    el.parentNode.replaceChild(elClone, el)